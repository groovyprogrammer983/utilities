/** This program replaces any missing values with a 0
	Instructions for use: 
	1. Input your matrix, formatting how you would expect
		E.g. 1 2 3
			 4 5 6
			 7 8 9
	2. Press enter one last time, inputting a final new line**/

#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

int main(){
	vector<vector<int>> v;
	unsigned int max_row_length = 0;

	while(true){
		string s;
		getline(cin, s);

		if(s.size() == 0){
			break;
		}

		stringstream stream(s);

		vector<int> v2;

		int n;
		while(stream >> n){
			v2.push_back(n);
		}

		max_row_length = max(max_row_length, v2.size());
		v.push_back(v2);
	}

	vector<vector<int>> v3(max_row_length);

	for(int i = 0; i < max_row_length; i++){
		v3[i].resize(v.size());
	}

	for(int i = 0; i < v.size(); i++){
		for(int j = 0; j < v[i].size(); j++){
			v3[j][i] = v[i][j];
		}
	}

	for(int i = 0; i < v3.size(); i++){
		for(int j = 0; j < v3[i].size(); j++){
			cout << v3[i][j];
			if(j < v3[i].size() - 1)
				cout << " ";
		}
		cout << endl;
	}
}